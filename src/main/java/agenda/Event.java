package agenda;

import java.time.*;
import java.time.temporal.ChronoUnit;

/**
 * Un évènement simple
 */
public class Event {

    /**
     * The title of this event
     */
    private final String myTitle;
    
    /**
     * The starting time of the event
     */
    private final LocalDateTime myStart;

    /**
     * The duration of the event
     */
    private final Duration myDuration;

    private Repetition myRepetition;

    /**
     * Constructs an event
     * @param title the title of this event
     * @param start the start time of this event
     * @param duration the duration of this event
     */
    public Event(String title, LocalDateTime start, Duration duration) {
        this.myTitle = title;
        this.myStart = start;
        this.myDuration = duration;
    }

    public void setRepetition(ChronoUnit frequency) {
        myRepetition = new Repetition(frequency);
    }

    public void addException(LocalDate date) {
        if (myRepetition != null) {
            myRepetition.addException(date);
        } else {
            throw new IllegalStateException("No repetition defined");
        }
    }

    public void setTermination(LocalDate terminationInclusive) {
        if (myRepetition != null) {
            myRepetition.setTermination(new Termination(myStart.toLocalDate(), myRepetition.getFrequency(), terminationInclusive));
        } else {
            throw new IllegalStateException("No repetition defined");
        }
    }

    public void setTermination(long numberOfOccurrences) {
        if (myRepetition != null) {
            myRepetition.setTermination(new Termination(myStart.toLocalDate(), myRepetition.getFrequency(), numberOfOccurrences));
        } else {
            throw new IllegalStateException("No repetition defined");
        }
    }

    /**
     * Tests if an event occurs on a given day
     * @param aDay the day to test
     * @return true if the event occurs on that day, false otherwise
     */
    public boolean isInDay(LocalDate aDay) {
        boolean result = Util.periodInDay(aDay, getStart(), getDuration());
        return result || (myRepetition != null && myRepetition.isInDay(this, aDay));
    }

    @Override
    public String toString() {
        return "%s{title=%s, start=%s, duration=%s}".formatted(this.getClass().getName(), getTitle(), getStart(), getDuration());
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return myTitle;
    }

    /**
     * @return the myStart
     */
    public LocalDateTime getStart() {
        return myStart;
    }


    /**
     * @return the myDuration
     */
    public Duration getDuration() {
        return myDuration;
    }

   public long getNumberOfOccurrences() {
        return myRepetition != null ? myRepetition.numberOfOccurrences() : 1;
   }

   public LocalDate getTerminationDate() {
        return myRepetition != null ? myRepetition.terminationDateInclusive() : myStart.plus(myDuration).toLocalDate();
   }
}

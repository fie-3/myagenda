package agenda;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class Repetition {
    public ChronoUnit getFrequency() {
        return myFrequency;
    }

    /**
     * Stores the frequency of this repetition, one of :
     * <UL>
     * <LI>ChronoUnit.DAYS for daily repetitions</LI>
     * <LI>ChronoUnit.WEEKS for weekly repetitions</LI>
     * <LI>ChronoUnit.MONTHS for monthly repetitions</LI>
     * </UL>
     */
    private final ChronoUnit myFrequency;

    // Les exceptions à la répétition
    private final List<LocalDate> myExceptions = new ArrayList<>();

    // La terminaison d'une répétition (optionnelle)
    private Termination myTermination;

    public Repetition(ChronoUnit myFrequency) {
        this.myFrequency = myFrequency;
    }

    public void addException(LocalDate date) {
        myExceptions.add(date);
    }

    public void setTermination(Termination termination) {
        myTermination = termination;
    }

    boolean isInDay(Event theEvent, LocalDate dayToTest) {
        if (myExceptions.contains(dayToTest)) {
            // Le jour à tester est une exception, pas la peine d'aller plus loin
            return false;
        }

        // On parcourt les occurrences successives de l'événement
        LocalDateTime nextTime = theEvent.getStart();
        LocalDate nextDate;
        do {
            if (Util.periodInDay(dayToTest, nextTime, theEvent.getDuration())) {
                return true; // On a trouvé
            }
            // Sinon on passe à l'occurrence suivante de l'événement
            nextTime = nextTime.plus(1, myFrequency);
            nextDate = nextTime.toLocalDate();
            // On s'arrête si :
            //    L'événement se termine avant la date de la prochaine occurrence
            // ou si :
            //    La prochaine occurrence est après le jour à tester
        } while (! terminatesBefore(nextDate) && ! nextDate.isAfter(dayToTest));
        // Logique Booléenne : ¬(A ∨ B) ≡ (¬A ∧ ¬B)
        return false;
    }

    /**
     * tests if this repetitive event will stop before a given date
     * @param day the date to test
     * @return true if this repetitive event stops before <code>date</code>,
     * false otherwise
     */

    boolean terminatesBefore(LocalDate day) {
        return myTermination != null && myTermination.terminatesBefore(day);
    }

    /**
     * Renvoie le nombre d'occurrences de cet événement répétitif
     * @return le nombre d'occurrences de cet événement répétitif ou -1 si l'événement ne se termine pas
     */
    public long numberOfOccurrences() {
        return myTermination != null ? myTermination.numberOfOccurrences() : -1;
    }

    /**
     * Renvoie la date de terminaison inclusive de cet événement répétitif
     * @return la date de terminaison inclusive de cet événement répétitif ou null si l'événement ne se termine pas
     */
    public LocalDate terminationDateInclusive() {
        return myTermination != null ? myTermination.terminationDateInclusive() : null;
    }
}

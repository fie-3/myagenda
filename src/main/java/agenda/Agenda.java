package agenda;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Description : An agenda that stores events
 */
public class Agenda {

    protected List<Event> myEvents = new LinkedList<>();

    /**
     * Adds an event to this agenda
     * @param e the event to add
     */
    public void addEvent(Event e) {
        myEvents.add(e);
    }

    /**
     * Computes the events that occur on a given day
     * @param day the day toi test
     * @return all events that occur on that day
     */
    public List<Event> eventsInDay(LocalDate day) {
        // En utilisant les stream et lambda-expressions
        return myEvents.stream()
            .filter(e -> e.isInDay(day))
            .collect(Collectors.toList());
        
        /* En java "classique"	
        List<Event> inDay = new LinkedList<>();
        for (Event e : myEvents) {
           if (e.isInDay(day))
                inDay.add(e);
        }
        return inDay;
         */
    }
    
    /**
     * Trouver les événements de l'agenda en fonction de leur titre
     * @param title le titre à rechercher
     * @return les événements qui ont le même titre
     */
    public List<Event> findByTitle(String title) {
        // TODO : implémenter cette méthode
        throw new UnsupportedOperationException("Pas encore implémenté");        
    }
    
    /**
     * Déterminer s'i 'il y a de la place dans l'agenda pour un évènement
     * @param e L'événement à tester (on se limitera aux événements simples).
     * @return vrai s'il y a de la place dans l'agenda pour cet événement
     */
    public boolean isFreeFor(Event e) {
        // TODO : implémenter cette méthode
        throw new UnsupportedOperationException("Pas encore implémenté");        
    }
}

package agenda;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Util {
    /**
     * Teste si une période définie par son début et sa durée est en intersection avec une journée
     * @param aDay le jour à tester
     * @param startOfPeriod le début de la période à tester
     * @param durationOfPeriod la durée de la période à tester
     * @return vrai s'il y a intersection entre la période et le jour
     */
    public static boolean periodInDay(LocalDate aDay, LocalDateTime startOfPeriod, Duration durationOfPeriod) {
        LocalDateTime endOfPeriod = startOfPeriod.plus(durationOfPeriod);
        LocalDateTime startOfDay = aDay.atStartOfDay();
        LocalDateTime endOfDay = startOfDay.plusDays(1);
        return !(startOfDay.isAfter(endOfPeriod) || endOfDay.isBefore(startOfPeriod));
    }

}

package agenda;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

public class Termination {

    public LocalDate terminationDateInclusive() {
        return myTerminationDate;
    }

    public long numberOfOccurrences() {
        return myNumberOfOccurrences;
    }

    /**
     * The date when this repetitive event ends
     */
    private final LocalDate myTerminationDate;

    /**
     * The number of occurrences of this event
     */
    private final long myNumberOfOccurrences;

    /**
     * Constructs a fixed termination event ending at a given date
     * @param start the start time of this event
     * @param frequency one of :
     * <UL>
     * <LI>ChronoUnit.DAYS for daily repetitions</LI>
     * <LI>ChronoUnit.WEEKS for weekly repetitions</LI>
     * <LI>ChronoUnit.MONTHS for monthly repetitions</LI>
     * </UL>
     * @param terminationInclusive the date when this event ends
     * @see java.time.temporal.ChronoUnit#between(Temporal, Temporal)
     */
    public Termination(LocalDate start, ChronoUnit frequency, LocalDate terminationInclusive) {
        myTerminationDate = terminationInclusive;
        /* On calcule le nombre d'occurrences à partir de la date de terminaison */
        myNumberOfOccurrences = frequency.between(start, terminationInclusive) + 1;
    }

    /**
     * Constructs a fixed termination event ending after a number of iterations
     * @param start the start time of this event
     * @param frequency one of :
     * <UL>
     * <LI>ChronoUnit.DAYS for daily repetitions</LI>
     * <LI>ChronoUnit.WEEKS for weekly repetitions</LI>
     * <LI>ChronoUnit.MONTHS for monthly repetitions</LI>
     * </UL>
     * @param numberOfOccurrences the number of occurrences of this repetitive event
     */
    public Termination(LocalDate start, ChronoUnit frequency, long numberOfOccurrences) {
        myNumberOfOccurrences = numberOfOccurrences;
        // On calcule la date de terminaison à partir du nombre d'occurrences
        myTerminationDate = start.plus(numberOfOccurrences - 1, frequency);
    }

    protected boolean terminatesBefore(LocalDate day) {
        return day.isAfter(myTerminationDate);
    }

}

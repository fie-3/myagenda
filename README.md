# Solution de l'examen 'Agenda'

Cf. le code pour l'implémentation.

## Solution de la question UML

La première approche utilise la relation de composition (has-a), la deuxième
utilise la relation d'héritage (is-a).

En complément de la [réponse générée par ChatGPT](./doc/Controverse_IsA_vs_HasA.md) on peut ajouter ceci ;

- Dans la solution avec héritage, un événement simple `SimpleEvent`ne pourra jamais devenir répétitif `RepetitiveEvent` (Un objet ne 
change jamais de classe)
- Dans la solution avec composition, un événement simple peut devenir répétitif en lui ajoutant une répétition via la méthode `setRepetition`.
